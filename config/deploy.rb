require "bundler/capistrano"

set :application, 'applicant2'
set :repository,  'https://rpaulokat@bitbucket.org/rpaulokat/applicant3.git'
set :scm, :git
set :scm_verbose, true

role :web, 'bewerbung.filmarche.de' 
role :app, 'bewerbung.filmarche.de'
role :db,  'bewerbung.filmarche.de', :primary => true 

set :deploy_to, '/var/www/bewerbung.filmarche.de/applicant'
set :user, 'rp'
set :group, 'www-data'

set :branch, 'master'
default_run_options[:pty] = true

after "deploy:restart", "deploy:cleanup"
after "deploy:setup", "dbconf:setup" 
after "deploy:finalize_update", "dbconf"

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end



namespace :dbconf do
  
  task :default do
    on_app
    on_db
  end
  
  desc "create a 'config' directory in shared_path for database.yml - to symlink it with everey deploy"
  task :setup do
    run "mkdir -p #{shared_path}/config"
    puts "you should place your database.yml into shared_path/config..."
  end

  desc "symlink database yml to prod-host"
  task :on_app, :roles => :app do
    puts "linking database.yml from shared_path to current on app"
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
  
  desc "symlink database yml to db-host"
  task :on_db, :roles => :db do
    puts "linking database.yml from shared_path to current on db"
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
end
