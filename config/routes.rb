Applicant2::Application.routes.draw do
  

  #resources :records
  get "sessions/new" => "sessions#new", :as => 'log_in'
  get "sessions/destroy" => "sessions#destroy", :as => 'log_out'
  
  get 'applicant_created/:id' => 'applicants#applicant_created', :as => 'applicant_created' 
  get 'confirm/:confirmation_token' => 'users#confirm', :as => 'confirmation' 
  get 'profile/' => 'users#profile', :as => 'profile'
  delete 'profile/portrait' => 'users#delete_portrait', :as => 'remove_portrait'
  delete 'rmfile/:att_type' => 'records#delete_attachment', :as => 'remove_attachment'
  
  get "admin/index" => "admin#index", :as => 'admin'
  get "admin/show_department/:id" => "admin#show_department", :as => "admin_show_department"
  get "admin/show_user/:id" => "admin#show_user", :as => "admin_show_user"
  put "admin/accept_user/:id" => "admin#accept_user", :as => "admin_accept_user"
  put "admin/reject_user/:id" => "admin#reject_user", :as => "admin_reject_user"
  put "admin/notify_complete/:id" => "admin#notify_complete", :as => 'admin_notify_complete_user'
  delete "admin/destroy_user/:id" => "admin#destroy_user", :as => "admin_destroy_user"
  get "admin/export_accepted" => "admin#export_accepted", :as => "accepted_as_csv"
  get "admin/export_rejected" => "admin#export_rejected", :as => "rejected_as_csv"
  
  resources :users do
    resource :record
  end

  namespace :admin do
    resources :users
  end
  #resources :records
  
  resources :sessions
  
  root :to => "applicants#index"
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
