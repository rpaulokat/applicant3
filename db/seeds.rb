# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Role.destroy_all
Department.destroy_all

["Regie", "Produktion","Kamera", "Montage", "Drehbuch","Dokumentarfilm"].each do |d|
  Department.create(:name => d)
end

%w{applicant admin}.each { |r| Role.create(:name => r) }
