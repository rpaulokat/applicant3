# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20160313175122) do

  create_table "departments", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "records", :force => true do |t|
    t.integer  "user_id",                    :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "common_task_file_name"
    t.string   "common_task_content_type"
    t.integer  "common_task_file_size"
    t.datetime "common_task_updated_at"
    t.string   "specific_task_file_name"
    t.string   "specific_task_content_type"
    t.integer  "specific_task_file_size"
    t.datetime "specific_task_updated_at"
    t.string   "image_archive_file_name"
    t.string   "image_archive_content_type"
    t.integer  "image_archive_file_size"
    t.datetime "image_archive_updated_at"
    t.string   "primary_file_name"
    t.string   "primary_content_type"
    t.integer  "primary_file_size"
    t.datetime "primary_updated_at"
    t.string   "video_url"
    t.string   "video_password"
    t.text     "comment"
    t.string   "video2_url"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "firstname",             :default => "",   :null => false
    t.string   "lastname",              :default => ""
    t.date     "date_of_birth"
    t.string   "phone"
    t.string   "mobile"
    t.string   "city"
    t.string   "postcode"
    t.string   "street"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.string   "email",                 :default => "",   :null => false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.integer  "department_id"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "portrait_file_name"
    t.string   "portrait_content_type"
    t.integer  "portrait_file_size"
    t.datetime "portrait_updated_at"
    t.integer  "role_id"
    t.boolean  "accepted"
    t.boolean  "notified_complete",     :default => true
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

end
