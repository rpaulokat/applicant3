class AddNotifyCompleteToUser < ActiveRecord::Migration
  def change
    add_column :users, :notified_complete, :boolean, default: true
  end
end
