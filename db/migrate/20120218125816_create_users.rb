class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :firstname, :null => false, :default => ""
      t.string :lastname, :nill => false, :default => ""
      #t.string :email
      t.date :date_of_birth
      t.string :phone
      t.string :mobile
      t.string :city
      t.string :postcode
      t.string :street

      t.timestamps
    end
  end
end
