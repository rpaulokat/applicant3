class AddVideoLinkAndPasswordToRecord < ActiveRecord::Migration
  def change
    add_column :records, :video_url, :string
    add_column :records, :video_password, :string
  end
end
