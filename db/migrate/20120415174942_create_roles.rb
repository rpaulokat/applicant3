class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :name

      t.timestamps
    end
    
    add_column :users, :role_id, :integer
    
    Role.reset_column_information
    User.reset_column_information
    
    ['admin', 'applicant'].each do |rolename|
      Role.create(:name => rolename)
    end
    
    _role = Role.find_by_name('applicant')
    
    User.all.each do |user|
      user.update_attribute(:role, _role)
    end
    
  end
end
