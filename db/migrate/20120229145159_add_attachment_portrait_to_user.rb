class AddAttachmentPortraitToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :portrait_file_name, :string
    add_column :users, :portrait_content_type, :string
    add_column :users, :portrait_file_size, :integer
    add_column :users, :portrait_updated_at, :datetime
    
    add_column :records, :common_task_file_name, :string
    add_column :records, :common_task_content_type, :string
    add_column :records, :common_task_file_size, :integer
    add_column :records, :common_task_updated_at, :datetime
    add_column :records, :specific_task_file_name, :string
    add_column :records, :specific_task_content_type, :string
    add_column :records, :specific_task_file_size, :integer
    add_column :records, :specific_task_updated_at, :datetime
    add_column :records, :image_archive_file_name, :string
    add_column :records, :image_archive_content_type, :string
    add_column :records, :image_archive_file_size, :integer
    add_column :records, :image_archive_updated_at, :datetime
    
  end

  def self.down
    remove_column :users, :portrait_file_name
    remove_column :users, :portrait_content_type
    remove_column :users, :portrait_file_size
    remove_column :users, :portrait_updated_at
    
    remove_column :records, :common_task_file_name
    remove_column :records, :common_task_content_type
    remove_column :records, :common_task_file_size
    remove_column :records, :common_task_updated_at
    remove_column :records, :specific_task_file_name
    remove_column :records, :specific_task_content_type
    remove_column :records, :specific_task_file_size
    remove_column :records, :specific_task_updated_at
    remove_column :records, :image_archive_file_name
    remove_column :records, :image_archive_content_type
    remove_column :records, :image_archive_file_size
    remove_column :records, :image_archive_updated_at
  end
end
