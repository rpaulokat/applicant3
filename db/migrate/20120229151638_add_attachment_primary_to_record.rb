class AddAttachmentPrimaryToRecord < ActiveRecord::Migration
  def self.up
    add_column :records, :primary_file_name, :string
    add_column :records, :primary_content_type, :string
    add_column :records, :primary_file_size, :integer
    add_column :records, :primary_updated_at, :datetime
  end

  def self.down
    remove_column :records, :primary_file_name
    remove_column :records, :primary_content_type
    remove_column :records, :primary_file_size
    remove_column :records, :primary_updated_at
  end
end
