/* Author:
 */
var myfunction = function(){

    var buttonHover = function(){
        $('button').hover(function(){
            $(this).animate({
                backgroundColor: 'rgba(200,200,230,0.6)'
            }, 'slow');
        }, function(){
            $(this).animate({
                backgroundColor: 'rgba(255,255,255,0.7)'
            }, 'fast')
        });
    };
    
    var contentHideShow = function(){
        $('nav').hide();
        $('.content').slideUp(0);
        $('#main h2').css('cursor', 'pointer');
        $('#main h2').bind('click', function(){
            var previousState = $(this).siblings('.content').css('display');
            $('h2 ~ .content').slideUp(300);
            if (previousState === 'none') {
                $(this).siblings('.content').slideDown(300);
            }
        });
    };
    
    var uploadZones = function(){
        $('.uploadZone').addClass('uploadZoneStyle');
        $('.uploadZone > form').wrap('<div class="status"></div>');
        $('.uploadZone form input').wrap('<div class="uploadLink"></div>');
        $('.uploadLink').append('<span>Datei öffnen</span>');
		 /*
$('.uploadZone').prepend('<div class="dropHint">PDF hier reinziehen</div>');
        $('.uploadZone').append('<div class="dropZone">DROP</div>');
        var dropZones = $('.dropzone').get();
        dropZones.forEach(function(){
            this.addEventListener('dragenter', function(e){
                alert('drop!');
                $(this).animate({
                    opacity: 0.8
                }, 'slow');
            })
        });
*/
        
    };
    
    $('document').ready(function(){
        uploadZones();
        buttonHover();
        contentHideShow();
    });
}();
