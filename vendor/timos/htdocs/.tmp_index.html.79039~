<!doctype html><!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
    <html class="no-js" lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,400italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Inika:700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/libs/modernizr-2.0.6.min.js">
        </script>
        <script>
            document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')
        </script>
    </head>
    <body>
        <div id="container">
            <header>
                <a href="#"><h1><img src="img/archeLogoEmboss.png" alt="filmArche Logo"><span class="secondLine">Bewerbung 2012</span></h1></a>
            </header>
            <nav>
                <ul>
                    <li>
                        <a href="#aboutArche">Was ist die Filmarche?</a>
                    </li>
                    <li>
                        <a href="#courses">Was kann man bei euch machen?</a>
                    </li>
                    <li>
                        <a href="#mitmachen">Ich will mitmachen</a>
                    </li>
                    <li>
                        <a href="#login">Kenn ich schon. Ich will mich einloggen</a>
                    </li>
                </ul>
            </nav>
            <div id="main" role="main">
                <section id="aboutArche">
                    <article>
                        <h2>Filmarche? Wie, wo, was?</h2>
                        <div class="content">
                            <p>
                                Die filmArche bietet eine fundierte dreijährige Ausbildung in den fünf Fachbereichen Drehbuch, Regie, Kamera, Montage/Schnitt und Produktion.
                            </p>
                            <p>
                                Ihre Besonderheit ist die Selbstbestimmung in der Lehre und die Selbstorganisation des Unterrichts. Die Kurse erarbeiten sich gemeinsam die Inhalte des Lehrplans und passen ihn an ihre Bedürfnisse an. Sie bestimmen, welche Schwerpunkte sie legen.
                            </p>
                            <p>
                                Es gibt keine festangestellten Dozent/innen. Jeder Kurs bekommt ein Budget von dem er einmal pro Monat einen externen Profi aus Theorie oder Filmpraxis in den Unterricht einladen kann. Zu Anfang werden die Kurse von den älteren Jahrgängen intensiv betreut und in die Selbstorganisation eingeführt.
                            </p>
                        </div>
                    </article>
                </section>
                <section id="courses" class="clearfix">
                    <article>
                        <h2>Und was kann man bei euch lernen?</h2>
                        <div class="content">
                            <p>
                                Eine Menge. Wir haben fünf Fachbereich und Du musst Dich für einen entscheiden
                            </p>
                            <ul>
                                <li>
                                    <h3>Regie</h3>
                                    <div class="content">
                                        <p>
                                            <em>Was gibt einem Film seine Identität?
                                                <br>
                                                Warum ist etwas tragisch, komisch, tragikomisch?
                                                <br>
                                                Wie kann man schnell mal was in Ruhe erzählen?
                                                <br>
                                                Welche Stimmung herrscht in der Welt, welche am Set?
                                            </em>
                                        </p>
                                        <p>
                                            Regie zu führen ist eine sowohl künstlerisch wie auch menschlich spannende, anspruchsvolle und herausfordernde Aufgabe, nicht nur, weil der Regisseur/die Regisseurin die künstlerische Leitung eines Filmteams übernimmt.
                                        </p>
                                        <p>
                                            Ausgehend von der inszenatorischen Werkinterpretation koordiniert die Regie die Kreativität der einzelnen Departments in eine bestimmte Richtung. Sie muss in der Rolle der Verantwortlichen nicht nur Überblick, Autorität und Nerven behalten, sondern dem Team auch als Ansprechpartnerin und Vertrauensperson permanent zur Seite stehen.
                                        </p>
                                        <p>
                                            Gleichzeitig ist die Arbeit mit den Schauspielern und Schauspielerinnen eine der wichtigsten Aufgaben der Regie.
                                        </p>
                                        <p>
                                            Im Rahmen des Studiums an der filmArche werden im Bereich Regie viele verschiedene Aspekte unterrichtet und Fähigkeiten vermittelt. Schauspielführung, Mise en Scène und Dramaturgie sind die drei großen Themenblöcke, die sich in regelmäßigen Abständen im Lehrplan wieder finden.
                                        </p>
                                        <p>
                                            Zusätzlich werden die Vorraussetzungen für kompetente Führung sowie filmisches Allgemeinwissen und die Fähigkeit zur konstruktiven Kommunikation mit den einzelnen Abteilungen während des gesamten Produktionsprozesses geschult.
                                        </p>
                                        <p>
                                            Die Studierenden werden sich daher auch mit Themen wie Psychologie am Set, Genre- und Filmgeschichte als auch den technischen Möglichkeiten und deren Neuerungen beschäftigen.
                                        </p>
                                        <p>
                                            Sämtliche Inhalte werden durch die Studierenden mithilfe zahlreicher Übungen und der Verwirklichung verschiedener Projekte während der gesamten Ausbildung zur Anwendung gebracht, um theoretisches Wissen zu festigen und praktische Kompetenzen zu erlangen.
                                        </p>
                                        <p>
                                            Wie alle anderen Lehrgänge an der filmArche gestalten auch die Regieklassen ihren Unterricht selbst. Als Orientierung dient das Curriculum Regie.
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <h3>Drehbuch</h3>
                                    <div class="content">
                                        <p>
                                            <em>Was erhebt eine Geschichte zum Filmstoff?
                                                <br>
                                                Wodurch entsteht Spannung?
                                                <br>
                                                Wie werden Charaktere zu Identifikationsfiguren?
                                                <br>
                                                Welche Inhalte machen einen Film unvergesslich?
                                            </em>
                                        </p>
                                        <p>
                                            Mit diesen und weiteren Fragen beschäftigt sich intensiv der Drehbuchlehrgang der filmArche: um starke Stoffe zu entwickeln und Drehbücher zu schreiben, die dann Grundlage des späteren Produktionsprozesses sind.
                                            Dabei gibt das Drehbuch dem Stoff eine Struktur, die später der Regie und Kamera sowohl Anhaltspunkte als auch Spielraum für die visuelle und darstellerische Umsetzung bietet.
                                        </p>
                                        <p>
                                            Die Drehbuchstudierenden entwickeln ein eigenes Repertoire an Ideen, Hintergründen und lebendigen Figuren, indem sie sich verschiedener Techniken wie Brainstorming, Visualisierungsübungen, Recherchen (Milieustudien, Interviews, Internet usw.) bedienen und diese Fertigkeiten in den individuellen, kreativen Schaffensprozess einzubinden lernen.
                                        </p>
                                        <p>
                                            An zahlreichen Film- und Drehbuchbeispielen werden neben dem klassischen dramatischen Dreiakter auch epische, episodenhafte, mythische und experimentelle Erzählformen untersucht.
                                            Gleichzeitig entwickelt der Drehbuchlehrgang eigene und gemeinsame Projekte kontinuierlich von der ersten Idee bis zum fertigen Drehbuch in verschiedenen Längen, Formaten und Genres.
                                        </p>
                                        <p>
                                            Wichtig ist jedoch nicht allein die Methodik, sondern auch der Inhalt der entstehenden Stoffe: die Studierenden der filmArche sollen mit ihren Geschichten das Leben aus unterschiedlichen Winkeln beleuchten und zu neuen Perspektiven finden.
                                        </p>
                                        <p>
                                            Deshalb wird sich der Drehbuchkurs auch mit filmgeschichtlichen und -gestalterischen Grundlagen, anderen Erzählformen wie Theater und Literatur auseinandersetzen und sich im dramaturgischen Handwerk üben. Seminare zu Berufskunde und Rechtsfragen runden den Kursplan ab und setzen das Gelernte in einen wirtschaftlich-rechtlichen Kontext.
                                        </p>
                                        <p>
                                            Wie alle anderen Lehrgänge an der filmArche gestalten auch die Drehbuchklassen ihren Unterricht selbst. Als Orientierung dient das Curriculum Drehbuch.
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <h3>Kamera</h3>
                                    <div class="content">
                                        <p>
                                            <em>Wie künstlich ist die Wirklichkeit?
                                                <br>
                                                Welches Licht dramatisiert einen Zwiespalt?
                                                <br>
                                                Wieviel muss man zeigen, um alles zu sehen?
                                                <br>
                                                Wann darf das Böse in Schönheit sterben?
                                            </em>
                                        </p>
                                        <p>
                                            Die Kamera präsentiert uns die Geschichte. Sie ist es, die die Perspektive auf den Kern des Filmes definiert, sie lenkt den Blick des Zuschauers und nimmt eine dezidierte Haltung zu den Figuren und zur Aussage des Filmes ein.
                                        </p>
                                        <p>
                                            Kameraleute bestimmen im Vorfeld des Drehs das Konzept und die Grundhaltung eines Filmes mit der Regie, wobei sie sowohl künstlerische und gestalterische Ziele als auch die technischen Möglichkeiten im Blick behalten. Während des Drehs leiten sie das visuelle Technikdepartment am Set und bündeln alle Ideen und Einschränkungen zu einem Stil.
                                        </p>
                                        <p>
                                            Der Lehrgang Kamera an der filmArche gliedert sich daher in zwei große und einen kleineren Themenbereich: Gestaltung und Technik sowie Organisation am Set.
                                        </p>
                                        <p>
                                            Bei der Kameratechnik wird der Schwerpunkt auf die zukunftsweisenden Entwicklungen der digitalen Aufzeichnungsverfahren und auf den kompetenten Umgang mit den vorhandenen gesetzt.
                                        </p>
                                        <p>
                                            Lichttechnik und Lichtgestaltung werden eng miteinander verknüpft, um die Theorie durch die Praxis zu verinnerlichen. Für die inhaltliche Auseinandersetzung mit filmischen Bildern soll als Grundlage ein fundiertes Wissen in Bildgestaltung, Kunst und Filmgeschichte erarbeitet werden. Die Organisation am Set wird zudem bei zahlreichen praktischen Gelegenheiten geschult werden können.
                                        </p>
                                        <p>
                                            Ziel ist, dass die Absolvent/innen der filmArche ihre persönliche filmische Bildersprache gefunden haben und zugleich in der Lage sind, sich auf das Publikum ihres Filmes einzustellen.
                                        </p>
                                        <p>
                                            Wie alle anderen Lehrgänge an der filmArche gestalten auch die Kameraklassen ihren Unterricht selbst. Als Orientierung dient das Curriculum Kamera
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <h3>Die Cutter</h3>
                                    <div class="content">
                                        <p>
                                            <em>Wie strukturiert man einen Film gegen die Norm?
                                                <br>
                                                Welche Bedeutung haben zufällig wirkende Bilder?
                                                <br>
                                                Wann erzählt man durch Weglassen mehr?
                                                <br>
                                                Warum verändert der Schnitt eine Haltung?
                                            </em>
                                        </p>
                                        <p>
                                            Schneiden, viel eher: Montieren, bedeutet, visuelle Geschichten und Bedeutungen zu erzählen und herauszugeben. In einem schöpferischen Prozess fertigt der Editor/die Editorin aus den Ingredienzien der Bilder, Geräusche und der Musik einen in sich stimmigen Film.
                                        </p>
                                        <p>
                                            Der Lehrgang Montage/Schnitt versteht sich als Labor – ein fächerübergreifendes Arbeitsfeld.
                                            Gemeinsam mit Regie, Kamera und Produktion forschen die Studierenden nach subjektiver und objektiver Film-Zeit, nach Szenen- und Gesamtrhythmus.
                                        </p>
                                        <p>
                                            Sie experimentieren mit unterschiedlichen Mustern und Metaphern der Filmsprache, mit Akzent und Ironie. Für ihre Arbeit brauchen sie sowohl Geduld und Präzision wie auch Musikalität und visuelle Vorstellungskraft, umfassendes technisches Wissen und dramaturgisches Handwerk.
                                        </p>
                                        <p>
                                            Ähnlich den Drehbuchautor/innen setzen sich auch die Studierenden des Montage/Schnitt mit Filmdramaturgie und -geschichte auseinander und ziehen Vergleiche zu klassischen Erzählformen wie Literatur und Theater.
                                        </p>
                                        <p>
                                            Ausgehend von einer fundierten technischen Basis (Digitalschnitt) werden sie gemeinsam mit den anderen Lehrgängen an Projekten arbeiten und eigenständig Filmszenen entwickeln.
                                        </p>
                                        <p>
                                            Seminare zu Themen wie Mischung, Digitales Compositing und elektronische Bildbearbeitung vervollständigen das Programm und bereiten die Studierenden auf die Zusammenarbeit mit anderen Departments der Postproduktion vor.
                                        </p>
                                        <p>
                                            Wie alle anderen Lehrgänge an der filmArche gestalten auch die Montageklassen ihren Unterricht selbst. Als Orientierung dient das Curriculum Montage/Schnitt.
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <h3>Produktion</h3>
                                    <div class="content">
                                        <p>
                                            <em>Was muss ein Film haben, um erfolgreich zu werden?
                                                <br>
                                                Womit kann man Menschen von seiner Idee überzeugen?
                                                <br>
                                                Wie lässt sich ohne Budget ein grosser Film realisieren?
                                                <br>
                                                Welche Art Filme wollen wir zeigen und vor allem warum?
                                            </em>
                                        </p>
                                        <p>
                                            Die Produzent/innen gestalten die Filmlandschaft durch Auswahl und Leitung von Projekten mit. Sie vermitteln zwischen den ideellen Ansprüchen der Regie und den Anforderungen, die Markt und Publikum an einen Film stellen. Künstlerische, geschäftliche und gesellschaftspolitische Erwägungen sind für die Produzent/innen an der filmArche dabei untrennbar miteinander verbunden.
                                        </p>
                                        <p>
                                            Unser Schwerpunkt liegt deshalb neben den Grundlagen der Filmfinanzierung und -organisation auf einer genauen Kenntnis der Medienlandschaft. Für diese Arbeit benötigen Produzent/innen Weitblick, Verantwortungsbewusstsein und Menschenkenntnis, aber auch ein gutes Gefühl für Geschichten und visuelle Gestaltung.
                                        </p>
                                        <p>
                                            Außerdem bedeutet Produzieren auch, durch eine stimmige Organisation künstlerische Arbeit zu ermöglichen. Das erfordert ein breites Basiswissen über alle Aspekte der Filmherstellung und -gestaltung.
                                        </p>
                                        <p>
                                            Wir werden uns einerseits mit Themen der Film- und Kunstgeschichte beschäftigen und in Fragen der Dramaturgie und Besetzung schulen; andererseits werden wir ganz praktisch Projekte realisieren. Im Austausch mit professionellen Produzent/innen, Firmen, Veranstaltern und Sendeanstalten knüpfen wir Kontakte und lernen, Filmproduktionen in betriebswirtschaftlicher Hinsicht (Kalkulation, Aquise, Verträge, Vertrieb etc.) abzuwickeln.
                                        </p>
                                        <p>
                                            In Zusammenarbeit mit den anderen Arbeitsgruppen initiieren und begleiten wir Projekte und geben den verschiedenen Departments ein kritisch-konstruktives Feedback.
                                        </p>
                                        <p>
                                            Wie alle anderen Lehrgänge an der filmArche gestalten auch die Produktionsklassen ihren Unterricht selbst. Als Orientierung dient das Curriculum Produktion.
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </article>
                </section>
                <section id="mitmachen">
                    <h2>Das klingt super! Ich will mitmachen? Wo kann ich?</h2>
                    <div class="content clearfix">
                        <div class="applicationIntro">
                            <p>
                                Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Nullam quis risus eget urna mollis ornare vel eu leo. Nulla vitae elit libero, a pharetra augue.
                            </p>
                            <p>
                                Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue.
                            </p>
                        </div>
                        <form action="index_submit" method="get" accept-charset="utf-8">
                            <label for="vorname">
                                Vorname
                            </label>
                            <input id="vorname" type="text">
                            <label for="name">
                                Name
                            </label>
                            <input type="text" id="name">
                            <label for="email">
                                Email
                            </label>
                            <input type="text" id="email">
                            <label for="telefon">
                                Telefon
                            </label>
                            <input type="text" id="telefon">
                            <label for="geburtstag">
                                Geburtstag
                            </label>
                            <input type="text" name="geburtstag" id="geburtstag">
                            <label for="department">
                                Fachbereich
                            </label>
                            <select name="department" id="department">
                                <option value="Drehbuch">Drehbuch</option>
                                <option value="Kamera">Kamera</option>
                                <option value="Montage">Montage/Schnitt</option>
                                <option value="Produktion">Produktion</option>
                                <option value="Regie">Regie</option>
                            </select>
                            <label for="captureDummy">
                                Capture Dummy
                            </label>
                            <input id="captureDummy" type="text">
                            <button type="submit">
                                Ich bin dabei!
                            </button>
                        </form>
                    </div>
                </section>
                <section id="login">
                    <div class="wrapper">
                        <form action="index_submit" method="get" accept-charset="utf-8" class="clearfix">
                            <input type="text" value="Deine E-Mail-Adresse" id="loginEmail"><input type="Password" value="Dein Passwort" id="loginPasswort">
                            <button type="submit">
                                Login!
                            </button>
                        </form>
                    </div>
                </section>
            </div>
            <aside>
                <a href="https://www.facebook.com/pages/filmArche/328954609917" target="_blank" id="facebookButton">Unsere Facebook-Seite</a>
                <a href="https://twitter.com/#!/filmArche" target="_blank" id="twitterButton">Wir bei twitter</a>
            </aside>
            <footer>
                <h4>Impressum</h4>
                <p>
                    filmArche e.V. 
                    Schlesische Straße 26 
                    10997 Berlin  
                    Telefon: +49 (0) 30 616 26 911 
                    Telefax: +49 (0) 30 364 61 780  
                    E-Mail: <a href="mailto:info@filmarche.de">info@filmarche.de</a>
                    Geschäftsführung: Inga Weyel
                    Vereinsregister: Amtsgericht Charlottenburg VR 21787
                </p>
            </footer>
        </div>
        <!--! end of #container -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js">
        </script>
        <script>
            window.jQuery || document.write('<script src="js/libs/jquery-1.6.2.min.js"><\/script>')
        </script>
        <script type="text/javascript" src="js/jqueryColor.js">
        </script>
		<script type="text/javascript" src="js/customSelect.jquery.js"></script>
        <script src="js/script.js">
        </script>
        <!-- end scripts-->
        <!--[if lt IE 7 ]>
            <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
            <script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
        <![endif]-->
    </body>
</html>
