FactoryGirl.define do
  factory :department do
    name { Faker::Company.bs }
    description { Faker::Lorem.paragraphs(4) }
  end
end
