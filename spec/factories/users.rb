# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    firstname { Faker::Name.first_name }
    lastname { Faker::Name.last_name }
    date_of_birth { '1977-10-18'}
    phone { Faker::PhoneNumber.phone_number }
    mobile { Faker::PhoneNumber.cell_phone }
    city { Faker::Address.city } 
    postcode { Faker::Address.zip_code }
    street { Faker::Address.street_name }
    confirmed_at { Date.today }
    department 
    role

    factory :admin_user do
      department nil
      association :role, factory: :admin_role
    end
  end
end
