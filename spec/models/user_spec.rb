require 'spec_helper'

describe User do
  it 'has a valid factory' do
    FactoryGirl.build(:user).should be_valid
  end

  it 'responds to class-method .amdins' do
    User.should respond_to(:admins)
  end
end
