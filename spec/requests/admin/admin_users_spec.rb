require 'spec_helper'

describe "Admin::Users" do
  describe "GET /admin_users" do

    before do
      admin = FactoryGirl.create(:admin_user, password: 'foobar', password_confirmation: 'foobar') 
      #session[:user_id] = admin.id 
      #request.env['HTTP_REFERER'] = '/'
      post sessions_path, :email => admin.email, :password => 'foobar'
    end

    it "works! (now write some real specs)" do
      #login
      get admin_users_path
      response.status.should be(200)
    end
  end
end
