require 'digest'
require 'csv'

class User < ActiveRecord::Base

  attr_accessor :password
 
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :firstname, :lastname, :phone, :department_id, :date_of_birth, :department, :password_confirmation, :portrait, :confirmed_at
  
  belongs_to :department
  has_one :record, :dependent => :destroy
  belongs_to :role
  has_attached_file :portrait, :styles => { :thumb => ["60x60", :jpg], :normal => ["200x200", :jpg] }, :dependent => :destroy
  
  validates_confirmation_of :password
  
  validates :email, :uniqueness => true
  validates :firstname, :lastname, :email, presence: true
  validates :phone, :date_of_birth, :department, :presence => true,  :unless => Proc.new { |user| user.role.name == 'admin' }
  validate :validate_minimum_age
  
  before_save :encrypt_password
  before_create :create_confirmation_token
  
  scope :admins, -> { joins(:role).where('roles.name=?', 'admin') }

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt) &&! user.confirmed_at.nil?
      user
    end
  end
  
  def name
    "#{firstname} #{lastname}"
  end
  
  def record_complete?
    self.record && self.record.complete?
  end
  
  def accepted?
    accepted ? true : false
  end
  
  def decision_made?
    accepted.nil? ? false : true
  end

  def fullname
    "#{firstname} #{lastname}"
  end
  
  private
  
  def create_confirmation_token
    self.confirmation_token = Digest::SHA2.new.hexdigest("foobar-#{rand(10000)}" << Time.now.to_s)
    #update_attribute(:confirmation_token, Digest::SHA2.new.hexdigest("foobar-" << Time.now.to_s))
  end
  
  def encrypt_password
    if self.password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
  
  def validate_minimum_age
    bd = date_of_birth
    min = Date.parse("1.10.#{Date.today.year-21}")
    begin
      if date_of_birth > min
        errors.add(:date_of_birth, "Mindestalter ist 21 Jahre")
      end
    rescue
      errors.add(:date_of_birth, "bitte in der form: 01.04.1900")
    end
  end
  
  def self.to_csv(users)
    csv_data = CSV.generate do |csv|
      csv << %w(id department lastname firstname email phone image)
      users.each do |user|
        csv << [user.id, user.department.name, user.lastname, user.firstname, user.email, user.phone, "http://bewerbung.filmarche.de#{user.portrait.url(:normal)}" ]
      end
    end
    return csv_data
  end
  
end
