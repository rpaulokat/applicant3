class Record < ActiveRecord::Base
  
  
  attr_accessible :primary, 
    :common_task, 
    :specific_task, 
    :image_archive, 
    :video_url, 
    :video2_url, 
    :video_password, 
    :comment
  
  has_attached_file :primary, :dependent => :destroy              # das ausgefüllte fpdf
  has_attached_file :common_task, :dependent => :destroy          # die allgemeinen bewerbungsaufgaben
  has_attached_file :specific_task, :dependent => :destroy        # fachspezifische bewerbungsaufgaben
  has_attached_file :image_archive, :dependent => :destroy        # fotoreihe als zip-archiv
  
  belongs_to :user
  
  before_save :check_text_length
  
  
  validates_attachment_content_type :primary, :content_type => "application/pdf"
  
  
  def attachments?
    return true if primary? || common_task? || specific_task? || image_archive?
  end
  
  def complete?
    missing_parts.empty? ? true : false
  end
  
  def missing_parts
    arr = []
    arr.push("Ausgefuellter Bewerbungsbogen") unless primary?
    arr.push("fachspezifische Aufgabe") unless specific_task?
    arr.push("allgemeine Aufgabe") unless common_task?
   
    if self.user.department 
        if self.user.department.name == "Kamera"
            arr.push("zip Archiv der Foto-Reihe") unless image_archive?
        end
    
        if self.user.department.name == "Montage"
            test = image_archive?
            unless test
                test = video_url
            end
            arr.push("Foto-Reihe oder Film") unless test
        end
    
        if self.user.department.name == "Dokumentarfilm"
            arr.push("Foto-Reihe") unless image_archive? 
            arr.push("Film") if video_url.nil?
        end
    
        if self.user.department.name == "Regie"
            arr.push("Film 1") if video_url.nil?
            arr.push("Film 2") if video2_url.nil?
        end
    end
    arr
    
  end
  
  
  private
  
  def check_text_length
    if self.video_url
      if self.video_url.empty?
        self.video_url = nil
      end
    end
    
  end
end
