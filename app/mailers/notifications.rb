class Notifications < ActionMailer::Base
  default from: "noreply@filmarche.de"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifications.confirmation.subject
  #
  def confirmation(user)
    @greeting = "#{user.firstname} #{user.lastname}"
    @confirmation_link = "http://bewerbung.filmarche.de/confirm/#{user.confirmation_token}"
    mail(:to => user.email, :subject => "FilmArche Bewerbung 2016")
  end

  def complete(user)
    @greeting = "#{user.firstname} #{user.lastname}"
    mail(to: user.email, subject: 'Filmarche Bewerbung 2016')
  end
end
