module AdminHelper
  
  def show_decision_status(user)
    if user.decision_made?
      user.accepted? ? "angenommen" : "abgelehnt"
    else
      "ausstehend"
    end
  end
end
