module UsersHelper
  
  def delete_links_for_attachments_on(record)
    hash = {}
    if record.primary?
      hash[:primary] = record.primary.path.split("/")[-1]
    end
    
    if record.common_task?
      hash[:common_task] = record.common_task.path.split("/")[-1]
    end
    
    if record.specific_task?
      hash[:specific_task] = record.specific_task.path.split("/")[-1]
    end
    
    if record.image_archive?
      hash[:image_archive] = record.image_archive.path.split("/")[-1]
    end    
    
    ret = ""
    hash.each_pair do |k,v|
      ret << content_tag(:li, truncate(v).concat(" ").concat( link_to('entfernen', remove_attachment_path(:user_id => record.user.id,:method => :delete, :confirm => "Datei loeschen?", :att_type => k)).html_safe) )  
    end
    ret    
  end
  
  def name_for_attachment(ppatt)
    ppatt.path.split("/")[-1]
  end
 
end
