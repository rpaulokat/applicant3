module ApplicantsHelper
  
  def missing_records_for(user)
    record = user.record || user.build_record
    record.missing_parts 
  end
  
end
