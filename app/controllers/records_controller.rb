class RecordsController < ApplicationController
  
  before_filter :set_user
  
  # GET /records
  # GET /records.json
  #def index
  #  @records = Record.all#

  #  respond_to do |format|
  #    format.html # index.html.erb
  #    format.json { render json: @records }
  #  end
  #end

  # GET /records/1
  # GET /records/1.json
  #def show
  #  @record = @user.record #Record.find(params[:id])#

  #  respond_to do |format|
  #    format.html # show.html.erb
  #    format.json { render json: @record }
  #  end
  #end

  # GET /records/new
  # GET /records/new.json
  #def new
  #  @record = Record.new

   # respond_to do |format|
   #   format.html # new.html.erb
   #   format.json { render json: @record }
   # end
  #end

  # GET /records/1/edit
  #def edit
  #  @record = Record.find(params[:id])
  #end

  # POST /records
  # POST /records.json
  def create
    @record = current_user.build_record(params[:record])

    respond_to do |format|
      if @record.save
        format.html { redirect_to profile_path, notice: 'Record was successfully created.' }
        format.json { render json: @record, status: :created, location: @record }
      else
        #format.html { render action: "new" }
        format.html { redirect_to :back, :alert => @record.errors }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /records/1
  # PUT /records/1.json
  def update
    @record = current_user.record
    #@record = @user.record

    respond_to do |format|
      if @record.update_attributes(params[:record])
        format.html { redirect_to profile_path, notice: 'Record was successfully updated.' }
        format.json { head :no_content }
      else
        #format.html { render action: "edit" }
        format.html { redirect_to profile_path, alert: "#{@record.errors.full_messages.join(" / ")}"}
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /records/1
  # DELETE /records/1.json
  def destroy
    @record = current_user.record # Record.find(params[:id])
    @record.destroy unless @record.nil?

    respond_to do |format|
      format.html { redirect_to profile_path }
      format.json { head :no_content }
    end
  end
  
  def delete_attachment
    record = current_user.record
    case params[:att_type]
    when 'primary'
      record.primary.destroy if record.primary?
    when 'common_task'
      record.common_task.destroy if record.common_task?
    when 'specific_task'
      record.specific_task.destroy if record.specific_task?
    when 'image_archive'
      record.image_archive.destroy if record.image_archive?
    end
    if record.save
      flash[:notice] = "Dokument entfernt"
    else
      flash[:alert] = "Konnte Dokument nicht entfernen"
    end
    redirect_to(profile_path)
  end

  private
  
  def set_user
    @user = params[:user_id] ? User.find(params[:user_id]) : User.new
  end

end

