class SessionsController < ApplicationController
  
  layout 'applicant'
  
  def new
  end
  
  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      flash[:notice] = "Hallo #{user.firstname}!"
      if user.role.name == 'applicant'
        redirect_to profile_path()
      end
      if user.role.name == 'admin'
        redirect_to admin_path()
      end

    else
      flash[:alert] = "Unbekannte Emailadresse oder falsches Passwort"
      redirect_to :back
    end
  end
  
  def destroy
    session[:user_id] = nil
    redirect_to root_path, :notice => 'abgemeldet...'
  end
end
