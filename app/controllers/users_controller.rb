class UsersController < ApplicationController
  layout 'applicant'
  #before_filter :authenticate_user!
  
  # GET /users
  # GET /users.json
  #def index
  #  @users = User.all

  #  respond_to do |format|
  #    format.html # index.html.erb
  #    format.json { render json: @users }
  #  end
  #end
  
  def profile
    @user = current_user || User.new
    @record = @user.record || @user.build_record
  end
  

  # GET /users/1
  # GET /users/1.json
  #def show
  #  @user = User.find(params[:id])#

  #  respond_to do |format|
  #    format.html # show.html.erb
  #    format.json { render json: @user }
  #  end
  #end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  #def edit
  #  @user = User.find(params[:id])
  #end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      _role  = Role.find_by_name('applicant')
      @user.role = _role
      if @user.save
        @user.reload
        n = Notifications.confirmation(@user)
        logger.debug("Users#create - Notification: #{n.inspect}")
        n.deliver
        format.html { redirect_to applicant_created_path(:id => @user.id), notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html do
          flash[:alert] = "Es sind Fehler aufgetreten, bitte sieh dir das Formular an und beachte die Hinweise!"
          render :action => 'new'
         end
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = current_user #User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to profile_path, notice: 'Daten aktualisiert' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = current_user # User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
  
  def delete_portrait
    @user = current_user
    @user.portrait.destroy
    if @user.save
      flash[:notice] = "Passbild geloescht"
      redirect_to profile_path
    else
      logger.error("Users#delete_portrait - error: #{@user.errors.inspect}")
      flash[:alert] = "Bild konnte nicht entfernt werden"
      redirect_to profile_path
    end
  end
  
  def confirm
    if user = User.find_by_confirmation_token(params[:confirmation_token])
      user.confirmed_at = Time.now
      respond_to do |format|
        if user.save
          logger.info("Applicants#confirm - user_id confirmed: #{user.id}")
          user.update_attribute(:confirmation_token, nil)
          session[:user_id] = user.id
          format.html { redirect_to profile_path, :notice => 'Email-Adresse bestaetigt' }
        else
          format.html {redirect_to root_path, :alert => "Email konnte nicht bestaetigt werden" }
        end
      end
    else
      logger.warn("Applicants#confirm - no such user by confirmation_token: #{params[:confirmation_token]}")
      flash[:alert] = "Kein solcher confirmation-code vorhanden"
      redirect_to root_path
    end
  end
  
end
