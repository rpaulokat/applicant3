require 'bcrypt'

class ApplicationController < ActionController::Base
  protect_from_forgery
  
  helper_method :current_user
  
  
  private

  def restrict_admin_access
    
    unless current_user.role.name == 'admin'
      redirect_to :back, :alert => "Access prohibited!"
    end
    
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
end
