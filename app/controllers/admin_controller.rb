class AdminController < ApplicationController
  
  layout 'applicant'
  
  before_filter :restrict_admin_access
  
  def index
    
  end
  
  def show_department
    @department = Department.find(params[:id])
  end
  
  def show_user
    @user = User.find(params[:id])
  end
  
  def accept_user
    @user = User.find(params[:id])
    @user.update_attribute(:accepted, false)
    @user.accepted= true
    @user.save 
    flash[:notice] = "User is accepted!"
    render action: 'show_user'
  end
  
  def reject_user
    @user = User.find(params[:id])
    @user.update_attribute(:accepted, false)
    flash[:notice] = "User is now rejected"
    render action: 'show_user'
  end
  
  def destroy_user
    @user = User.find(params[:id])
    dept = @user.department
    if @user.accepted?
      flash[:error] = "Der/die Bewerberin ist angenommen - du kannst sie/ihn nicht loeschen"
      redirect_to :back and return
    end
    if @user.destroy
      flash[:notice] = "User entfernt"
    else
      flash[:error] = "Konnte user nicht loeschen"
    end
    redirect_to admin_show_department_path(:id => dept.id)
  end
  

  def notify_complete
    @user = User.find(params[:id])
    if @user.notified_complete == true
      flash[:error] = "Bewerber/in wurde bereits ueber Vollstaendigkeit benachrichtigt..."
      redirect_to :back and return 
    else
      @user.notified_complete = true
      @user.save
      Notifications.complete(@user).deliver
      flash[:notice] = "Hinweismail ueber Vollstaendigkeit an Bewerber/in versendet"
      redirect_to :back
    end

  end
  
  def export_accepted
    #users = export_all(true)
    data = User.to_csv(export_all(true))
    fname = "Angenommene_Bewerberinnen_#{DateTime.now.to_i}"
    send_data data, 
              :type => 'text/csv; charset=utf8; header=present',
              :disposition => "attachment; filename=#{fname}.csv"
    
  end
  
  def export_rejected
    data = User.to_csv(export_all(false))
    fname = "Abgelehnte_Bewerberinnen_#{DateTime.now.to_i}"
    send_data data, 
              :type => 'text/csv; charset=utf8; header=present',
              :disposition => "attachment; filename=#{fname}.csv"
  end
  
  private
  
  def export_all(accepted = true)
    User.where("accepted=?", accepted).order(:department_id)
  end
  
  def restrict_access
    
    unless current_user.role.name == 'admin'
      redirect_to :back, :alert => "Access prohibited!"
    end
    
  end
  
end
